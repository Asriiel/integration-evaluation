import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class GeocoderProvider {

  constructor(public http: HttpClient) {
    console.log('Hello GeocoderProvider Provider');
  }

  getGeocode(ville, token){
    return new Promise(resolve => {
      this.http.get("https://api.mapbox.com/geocoding/v5/mapbox.places/"+ ville +".json?access_token="+token).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

}
