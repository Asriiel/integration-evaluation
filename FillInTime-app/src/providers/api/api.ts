import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiProvider {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  constructor(public http: HttpClient) {
    console.log('Hello ApiProvider');
  }


  /**
   * Récupère la liste des stations
   */
  getAllStation(){
    return new Promise(resolve => {
      this.http.get("http://apifillintime.tp.crea.pro/public/index.php/api/stations?page=1", this.httpOptions).subscribe(data => {
        resolve(data["hydra:member"]);
      }, err => {
        console.log(err);
      });
    });
  }

  /**
   * Récupère une station à partir de son ID
   * @param id
   */
  getStation(id){
    return new Promise(resolve => {
      this.http.get("http://apifillintime.tp.crea.pro/public/index.php/api/stations/"+id, this.httpOptions).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  /**
   * Récupère la liste de prix du pétrole brut
   */
  getCrudeOilPrice(){
    return new Promise(resolve => {
      this.http.get("http://apifillintime.tp.crea.pro/public/index.php/api/petroles?page=1", this.httpOptions).subscribe(data => {
        resolve(data["hydra:member"]);
      }, err => {
        console.log(err);
      });
    });
  }
}
