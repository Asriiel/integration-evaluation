import { Component } from '@angular/core';
import { IonicPage} from 'ionic-angular';
import mapboxgl from 'mapbox-gl';
import {GeocoderProvider} from "../../providers/geocoder/geocoder";
import {ApiProvider} from "../../providers/api/api";
@IonicPage()
@Component({
  selector: 'page-maps',
  templateUrl: 'maps.html',
})
export class MapsPage {
  token : string;
  constructor(
    public geocoder: GeocoderProvider,
    public api: ApiProvider
  ) {
    this.token = mapboxgl.accessToken = "pk.eyJ1IjoiYXNyaWllbCIsImEiOiJjamFueXJjN3c0aGc1MzNqdTMxN3NkZjI5In0.rQxFthw0Gy0-fCv6m8Zj6g";
  }

  ionViewDidLoad() {
    this.initMap();
    // Exemple d'utilisation du Geocoder MapBox
    this.geocoder.getGeocode("Angers", this.token).then(function (data) {
      console.log("Longitude : "+data['features'][0]['center'][0]);
      console.log("Latitude : "+data['features'][0]['center'][1]);
    });
  }

  initMap(){
    let maps = null;
    let that = this;
    // Récupération de la geolocation de l'utilisateur
    navigator.geolocation.getCurrentPosition(function (pos) {
      maps = new mapboxgl.Map({
        container: 'maps',
        center: [pos.coords.longitude, pos.coords.latitude],
        zoom: 5,
        pitch: 60,
        minZoom: 6,
        style: 'mapbox://styles/mapbox/dark-v9'
      });
      let latLngs = [];
      that.api.getAllStation().then(function (stations) {
        console.log(stations);
        // @ts-ignore
        /*_.forEach(stations, function (station) {
          let popup = new mapboxgl.Popup({ offset: 40, className: 'my-class' })
            .setHTML('<div class="content">\n' +
              '    <div class="header"><p>' + station.name + '</p></div>\n' +
              '    <div class="header"><p>' + station.cp + ', ' + station.ville + ' </p></div>\n' +
              '    <hr />\n' +
              '    <div class="footer"><button onclick="getEventModal('+ station.id +')" class="go-to">Visualiser </button></div>\n' +
              '</div>');

          latLngs.push([station.longitude, station.latitude]);
          let el = document.createElement('div');
          el.className = 'marker';
          latLngs.push({
            "type": "Feature",
            "properties": {},
            "geometry": {
              "type": "Point",
              "coordinates": [
                station.longitude,
                station.latitude
              ]
            }
          });
          new mapboxgl.Marker(el)
            .setLngLat([station.longitude, station.latitude])
            .setPopup(popup)
            .addTo(maps);

        });*/

      });
    }, function (err) {
      console.log(err);
    });

  }

}
