'use strict';

$(document).ready(function(){

    $('.tabs .SelectInfos-bloc').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('.tabs .SelectInfos-bloc').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    });

});

