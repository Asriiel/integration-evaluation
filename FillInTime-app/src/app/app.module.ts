import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { MapsPage } from "../pages/maps/maps";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GeocoderProvider } from '../providers/geocoder/geocoder';
import {FiltreComponent} from "../components/filtre/filtre";
import {MenuComponent} from "../components/menu/menu";
import { SearchComponent } from "../components/search/search";
import { ModalComponent } from "../components/modal/modal";
import { ApiProvider } from '../providers/api/api';
import { MapsProvider } from '../providers/maps/maps';

@NgModule({
  declarations: [
    MyApp,
    MapsPage,
    FiltreComponent,
    MenuComponent,
    ModalComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MapsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GeocoderProvider,
    ApiProvider,
    MapsProvider
  ]
})
export class AppModule {}
