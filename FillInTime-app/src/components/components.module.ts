import { NgModule } from '@angular/core';
import {Component} from "@angular/core";
import { FiltreComponent } from './filtre/filtre';
import { MenuComponent } from './menu/menu';
import { SearchComponent } from './search/search';
import { ModalComponent } from './modal/modal';
@NgModule({
	declarations: [FiltreComponent,
    MenuComponent,
    SearchComponent,
    ModalComponent],
	imports: [Component],
	exports: [FiltreComponent,
    MenuComponent,
    SearchComponent,
    ModalComponent]
})
export class ComponentsModule {}
