import { Component } from '@angular/core';

/**
 * Generated class for the FiltreComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'filtre',
  templateUrl: 'filtre.html'
})
export class FiltreComponent {

  text: string;
  autour: string;
  france: string;
  sp95: string;
  sp98: string;
  sp95e10: string;
  gazole: string;
  moinscher: string;
  services: string;
  nocritere: string;
  expandFilter: boolean;

  constructor() {
    this.autour = 'Autour de moi';
    this.france = 'France entière';
    this.sp95 = 'Sans Plomb 95';
    this.sp98 = 'Sans Plomb 98';
    this.sp95e10 = "SP95 - E10";
    this.gazole = "Gazole";
    this.nocritere = 'Aucun critère';
    this.moinscher = 'Moins cher';
    this.services = 'Services';
    this.expandFilter = true;
  }
  switchFilter() {
    this.expandFilter = this.expandFilter != true;
  }
}
