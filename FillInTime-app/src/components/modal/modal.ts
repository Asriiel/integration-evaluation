import { Component } from '@angular/core';
import { ApiProvider } from "../../providers/api/api";

/**
 * Generated class for the ModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'modal',
  templateUrl: 'modal.html'
})
export class ModalComponent {

  text: string;
  openClose: boolean;

  constructor(
    public api: ApiProvider
  ) {
    console.log('Hello ModalComponent Component');
    this.text = 'Hello World';
    this.openClose = true;
    let that = this;
    document.addEventListener("openModal",function (e) {
      // @ts-ignore
      console.log(e.detail.id);
      that.openCloseModal();
      // @ts-ignore
      that.fetchDataStation(e.detail.id);
    });
  }

  openCloseModal() {
    this.openClose = this.openClose != true;
  }

  fetchDataStation(id){
    this.api.getStation(id).then(function (data) {
      console.log(data);
    });
  }

  ionViewLoaded(){

  }



}
