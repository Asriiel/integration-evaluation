var chart    = document.getElementById('chart').getContext('2d'),

    gradient = chart.createLinearGradient(0, 0, 0, 450);
gradient.addColorStop(0, 'rgba(255, 187, 114, 0.9)');
gradient.addColorStop(0.5, 'rgba(255, 82, 78, 0.6)');
gradient.addColorStop(1, 'rgba(255, 0, 0, 0)');

gradient2 = chart.createLinearGradient(0, 0, 0, 450);
gradient2.addColorStop(0, 'rgba(255, 187, 114, 0.75)');
gradient2.addColorStop(0.5, 'rgba(255, 82, 78, 0.5)');
gradient2.addColorStop(1, 'rgba(255, 0, 0, 0)');

25, 26, 26

var data  = {
    labels: [ 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre', 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin' ],
    datasets: [{
        label: 'Evolution du prix du carburant de cette station',
        backgroundColor: gradient,
        pointBackgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#ff524e',
        data: [50, 55, 80, 110, 54, 50,50, 55, 80, 110, 54, 50,50, 55, 80, 110, 54, 50]
    }, {
        label: 'Evolution du prix du pétrole brut',
        backgroundColor: 'rgba(255, 0, 0, 0)',
        pointBackgroundColor: 'white',
        borderWidth: 2,
        borderColor: '#ff524e',
        data: [40, 45, 50, 80, 80, 30,40, 45, 50, 80, 80, 30,40, 45, 50, 80, 80, 30]
    }]
};


var options = {
    responsive: true,
    maintainAspectRatio: true,
    animation: {
        easing: 'easeInOutQuad',
        duration: 520
    },
    scales: {
        xAxes: [{
            gridLines: {
                color: 'rgba(200, 200, 200, 0.05)',
                lineWidth: 1
            }
        }],
        yAxes: [{
            gridLines: {
                color: 'rgba(200, 200, 200, 0.08)',
                lineWidth: 1
            }
        }]
    },
    elements: {
        line: {
            tension: 0.4
        }
    },
    legend: {
        display: true
    },
    point: {
        backgroundColor: 'white'
    },
    tooltips: {
        titleFontFamily: 'Open Sans',
        backgroundColor: 'rgba(0,0,0,0.3)',
        titleFontColor: 'red',
        caretSize: 5,
        cornerRadius: 2,
        xPadding: 10,
        yPadding: 10
    }
};


var chartInstance = new Chart(chart, {
    type: 'line',
    data: data,
    options: options
});