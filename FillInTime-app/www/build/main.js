webpackJsonp([1],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_mapbox_gl__ = __webpack_require__(251);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_mapbox_gl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_mapbox_gl__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_geocoder_geocoder__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_api__ = __webpack_require__(77);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MapsPage = /** @class */ (function () {
    function MapsPage(geocoder, api) {
        this.geocoder = geocoder;
        this.api = api;
        this.token = __WEBPACK_IMPORTED_MODULE_1_mapbox_gl___default.a.accessToken = "pk.eyJ1IjoiYXNyaWllbCIsImEiOiJjamFueXJjN3c0aGc1MzNqdTMxN3NkZjI5In0.rQxFthw0Gy0-fCv6m8Zj6g";
    }
    MapsPage.prototype.ionViewDidLoad = function () {
        this.initMap();
        // Exemple d'utilisation du Geocoder MapBox
        this.geocoder.getGeocode("Angers", this.token).then(function (data) {
            console.log("Longitude : " + data['features'][0]['center'][0]);
            console.log("Latitude : " + data['features'][0]['center'][1]);
        });
    };
    MapsPage.prototype.initMap = function () {
        var maps = null;
        var that = this;
        // Récupération de la geolocation de l'utilisateur
        navigator.geolocation.getCurrentPosition(function (pos) {
            maps = new __WEBPACK_IMPORTED_MODULE_1_mapbox_gl___default.a.Map({
                container: 'maps',
                center: [pos.coords.longitude, pos.coords.latitude],
                zoom: 5,
                pitch: 60,
                minZoom: 6,
                style: 'mapbox://styles/mapbox/dark-v9'
            });
            var latLngs = [];
            that.api.getAllStation().then(function (stations) {
                console.log(stations);
                // @ts-ignore
                /*_.forEach(stations, function (station) {
                  let popup = new mapboxgl.Popup({ offset: 40, className: 'my-class' })
                    .setHTML('<div class="content">\n' +
                      '    <div class="header"><p>' + station.name + '</p></div>\n' +
                      '    <div class="header"><p>' + station.cp + ', ' + station.ville + ' </p></div>\n' +
                      '    <hr />\n' +
                      '    <div class="footer"><button onclick="getEventModal('+ station.id +')" class="go-to">Visualiser </button></div>\n' +
                      '</div>');
        
                  latLngs.push([station.longitude, station.latitude]);
                  let el = document.createElement('div');
                  el.className = 'marker';
                  latLngs.push({
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                      "type": "Point",
                      "coordinates": [
                        station.longitude,
                        station.latitude
                      ]
                    }
                  });
                  new mapboxgl.Marker(el)
                    .setLngLat([station.longitude, station.latitude])
                    .setPopup(popup)
                    .addTo(maps);
        
                });*/
            });
        }, function (err) {
            console.log(err);
        });
    };
    MapsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-maps',template:/*ion-inline-start:"C:\Users\jessy\Documents\projets\fillintime\FillInTime-app\src\pages\maps\maps.html"*/'<!--\n\n  Generated template for the MapsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n\n\n<ion-content>\n\n  <filtre></filtre>\n\n  <search></search>\n\n  <menu></menu>\n\n  <modal></modal>\n\n  <div id="maps"></div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\jessy\Documents\projets\fillintime\FillInTime-app\src\pages\maps\maps.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__providers_geocoder_geocoder__["a" /* GeocoderProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_geocoder_geocoder__["a" /* GeocoderProvider */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__providers_api_api__["a" /* ApiProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_api_api__["a" /* ApiProvider */]) === "function" && _b || Object])
    ], MapsPage);
    return MapsPage;
    var _a, _b;
}());

//# sourceMappingURL=maps.js.map

/***/ }),

/***/ 113:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 113;

/***/ }),

/***/ 155:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/maps/maps.module": [
		282,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 155;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeocoderProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GeocoderProvider = /** @class */ (function () {
    function GeocoderProvider(http) {
        this.http = http;
        console.log('Hello GeocoderProvider Provider');
    }
    GeocoderProvider.prototype.getGeocode = function (ville, token) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get("https://api.mapbox.com/geocoding/v5/mapbox.places/" + ville + ".json?access_token=" + token).subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    GeocoderProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], GeocoderProvider);
    return GeocoderProvider;
}());

//# sourceMappingURL=geocoder.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(221);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_maps_maps__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_geocoder_geocoder__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_filtre_filtre__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_menu_menu__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_search_search__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_modal_modal__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_api_api__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_maps_maps__ = __webpack_require__(281);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_maps_maps__["a" /* MapsPage */],
                __WEBPACK_IMPORTED_MODULE_9__components_filtre_filtre__["a" /* FiltreComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_menu_menu__["a" /* MenuComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_modal_modal__["a" /* ModalComponent */],
                __WEBPACK_IMPORTED_MODULE_11__components_search_search__["a" /* SearchComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/maps/maps.module#MapsPageModule', name: 'MapsPage', segment: 'maps', priority: 'low', defaultHistory: [] }
                    ]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_maps_maps__["a" /* MapsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_8__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
                __WEBPACK_IMPORTED_MODULE_13__providers_api_api__["a" /* ApiProvider */],
                __WEBPACK_IMPORTED_MODULE_14__providers_maps_maps__["a" /* MapsProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_maps_maps__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, menu, statusBar, splashScreen) {
        this.platform = platform;
        this.menu = menu;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        // make HelloIonicPage the root (or first) page
        this.rootPage = __WEBPACK_IMPORTED_MODULE_2__pages_maps_maps__["a" /* MapsPage */];
        this.initializeApp();
        // set our app's pages
        this.pages = [
            { title: 'Maps', component: __WEBPACK_IMPORTED_MODULE_2__pages_maps_maps__["a" /* MapsPage */] }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.backgroundColorByHexString('#191A1A');
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // close the menu when clicking a link from the menu
        this.menu.close();
        // navigate to the new page if it is not the current page
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\jessy\Documents\projets\fillintime\FillInTime-app\src\app\app.html"*/'<ion-menu [content]="content">\n\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Pages</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"C:\Users\jessy\Documents\projets\fillintime\FillInTime-app\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FiltreComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the FiltreComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var FiltreComponent = /** @class */ (function () {
    function FiltreComponent() {
        this.autour = 'Autour de moi';
        this.france = 'France entière';
        this.sp95 = 'Sans Plomb 95';
        this.sp98 = 'Sans Plomb 98';
        this.sp95e10 = "SP95 - E10";
        this.gazole = "Gazole";
        this.nocritere = 'Aucun critère';
        this.moinscher = 'Moins cher';
        this.services = 'Services';
        this.expandFilter = true;
    }
    FiltreComponent.prototype.switchFilter = function () {
        this.expandFilter = this.expandFilter != true;
    };
    FiltreComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'filtre',template:/*ion-inline-start:"C:\Users\jessy\Documents\projets\fillintime\FillInTime-app\src\components\filtre\filtre.html"*/'<div class="ArrowButton ArrowButton--mobile"  (click)="switchFilter()">\n\n    <span></span>\n\n</div>\n\n<div class="AsideBloc" [ngClass]="{\'expandFilter\': expandFilter}">\n\n    <div class="ArrowButton" [ngClass]="{\'expandFilter\': !expandFilter}" (click)="switchFilter()">\n\n        <span></span>\n\n    </div>\n\n    <div class="AsideBloc--flex">\n\n        <div class="AsideBloc-loca">\n\n            <span class="AsideBloc-loca-background">Localisation</span>\n\n            <h2>Localisation</h2>\n\n            <div class="Selectors">\n\n                <div class="Selector">\n\n                    <input type="checkbox" id="aroundme" name="aroundme">\n\n                    <label for="aroundme">{{ autour }}</label>\n\n                </div>\n\n                <div class="Selector">\n\n                    <input type="checkbox" id="france" name="france">\n\n                    <label for="france">{{ france }}</label>\n\n                </div>\n\n            </div>\n\n        </div>\n\n        <div class="AsideBloc-carburant">\n\n            <span class="AsideBloc-carburant-background">Carburants</span>\n\n            <h2>Carburants</h2>\n\n            <div class="Selectors">\n\n                <div class="Selector">\n\n                    <input type="checkbox" id="{{ sp95 }}" name="{{ sp95 }}">\n\n                    <label for="{{ sp95 }}">{{ sp95 }}</label>\n\n                </div>\n\n                <div class="Selector">\n\n                    <input type="checkbox" id="{{ sp98 }}" name="{{ sp98 }}">\n\n                    <label for="{{ sp98 }}">{{ sp98 }}</label>\n\n                </div>\n\n                <div class="Selector">\n\n                    <input type="checkbox" id="{{ sp95e10 }}" name="{{ sp95e10 }}">\n\n                    <label for="{{ sp95e10 }}">{{ sp95e10 }}</label>\n\n                </div>\n\n                <div class="Selector">\n\n                    <input type="checkbox" id="{{ gazole }}" name="{{ gazole }}">\n\n                    <label for="{{ gazole }}">{{ gazole }}</label>\n\n                </div>\n\n            </div>\n\n        </div>\n\n        <div class="AsideBloc-searchby">\n\n            <span class="AsideBloc-searchby-background">Recherche</span>\n\n            <h2>Recherche par</h2>\n\n            <div class="Selectors">\n\n                <div class="Selector">\n\n                    <input type="checkbox" id="{{ moinscher }}" name="{{ moinscher }}">\n\n                    <label for="{{ moinscher }}">{{ moinscher }}</label>\n\n                </div>\n\n                <div class="Selector">\n\n                    <input type="checkbox" id="{{ services }}" name="{{ services }}">\n\n                    <label for="{{ services }}">{{ services }}</label>\n\n                </div>\n\n                <div class="Selector">\n\n                    <input type="checkbox" id="{{ nocritere }}" name="{{ nocritere }}">\n\n                    <label for="{{ nocritere }}">{{ nocritere }}</label>\n\n                </div>\n\n            </div>\n\n        </div>\n\n        <a class="ButtonFilter" href="#">Rechercher</a>\n\n    </div>\n\n</div>\n\n'/*ion-inline-end:"C:\Users\jessy\Documents\projets\fillintime\FillInTime-app\src\components\filtre\filtre.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], FiltreComponent);
    return FiltreComponent;
}());

//# sourceMappingURL=filtre.js.map

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the MenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var MenuComponent = /** @class */ (function () {
    function MenuComponent() {
        this.expendMenu = false;
    }
    MenuComponent.prototype.switchMenu = function () {
        this.expendMenu = this.expendMenu != true;
    };
    MenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'menu',template:/*ion-inline-start:"C:\Users\jessy\Documents\projets\fillintime\FillInTime-app\src\components\menu\menu.html"*/'<div class="Menu">\n\n  <div class="menu-icon">\n\n    <span class="menu-icon__line menu-icon__line-left"></span>\n\n    <span class="menu-icon__line"></span>\n\n    <span class="menu-icon__line menu-icon__line-right"></span>\n\n  </div>\n\n\n\n  <div style="z-index: 30" class="nav">\n\n    <div class="nav__content">\n\n      <div class="background nav__circle">\n\n        <div class="circles circle1"></div>\n\n        <div class="circles circle2"></div>\n\n        <div class="circles circle4"></div>\n\n        <div class="circles circle5"></div>\n\n        <div class="circles circle6"></div>\n\n        <div class="circles circle7"></div>\n\n      </div>\n\n      <ul class="nav__list">\n\n        <li class="nav__list-item">Accueil</li>\n\n        <li class="nav__list-item">Visualisation</li>\n\n        <li class="nav__list-item">Contact</li>\n\n      </ul>\n\n    </div>\n\n  </div>\n\n</div>\n\n'/*ion-inline-end:"C:\Users\jessy\Documents\projets\fillintime\FillInTime-app\src\components\menu\menu.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], MenuComponent);
    return MenuComponent;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the SearchComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var SearchComponent = /** @class */ (function () {
    function SearchComponent() {
    }
    SearchComponent.prototype.search = function () {
        console.log(this.content);
    };
    SearchComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'search',template:/*ion-inline-start:"C:\Users\jessy\Documents\projets\fillintime\FillInTime-app\src\components\search\search.html"*/'<!-- Generated template for the SearchComponent component -->\n\n<div>\n\n  <div class="containerForm">\n\n    <form action="test.html" method="post">\n\n      <input [(ngModel)]="content" placeholder="RECHERCHER UNE VILLE" type="text" id="search" name="search">\n\n      <button (click)="search()" class="buttonSearch"><i class="fas fa-search"></i></button>\n\n    </form>\n\n  </div>\n\n</div>\n\n'/*ion-inline-end:"C:\Users\jessy\Documents\projets\fillintime\FillInTime-app\src\components\search\search.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], SearchComponent);
    return SearchComponent;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_api_api__ = __webpack_require__(77);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ModalComponent = /** @class */ (function () {
    function ModalComponent(api) {
        this.api = api;
        console.log('Hello ModalComponent Component');
        this.text = 'Hello World';
        this.openClose = true;
        var that = this;
        document.addEventListener("openModal", function (e) {
            // @ts-ignore
            console.log(e.detail.id);
            that.openCloseModal();
            // @ts-ignore
            that.fetchDataStation(e.detail.id);
        });
    }
    ModalComponent.prototype.openCloseModal = function () {
        this.openClose = this.openClose != true;
    };
    ModalComponent.prototype.fetchDataStation = function (id) {
        this.api.getStation(id).then(function (data) {
            console.log(data);
        });
    };
    ModalComponent.prototype.ionViewLoaded = function () {
    };
    ModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'modal',template:/*ion-inline-start:"C:\Users\jessy\Documents\projets\fillintime\FillInTime-app\src\components\modal\modal.html"*/'<div class="Modal" [ngClass]="{\'openClose\': openClose}">\n\n  <div class="Modal-box">\n\n    <span [ngClass]="{\'openClose\': !openClose}" (click)="openCloseModal()">Close</span>\n\n    <div class="Modal-box-first">\n\n      <h2>Géant Casino (Le Puy-en-Velay)</h2>\n\n\n\n      <div class="CardChart">\n\n\n\n        <div class="CardChart-area" style="width: 100%; height: 220px; position: relative;">\n\n          <canvas id="chartBig1"></canvas>\n\n        </div>\n\n\n\n      </div>\n\n\n\n              <div class="Carburants" data-toggle="buttons">\n\n                <label class="Carburant" id="0">\n\n                  <input type="radio" name="options" autocomplete="off" checked> Accounts\n\n                </label>\n\n                <label class="Carburant" id="1">\n\n                  <input type="radio" name="options" autocomplete="off"> Purchases\n\n                </label>\n\n                <label class="Carburant" id="2">\n\n                  <input type="radio" name="options" autocomplete="off"> Sessions\n\n                </label>\n\n              </div>\n\n\n\n\n\n\n\n\n\n      <div class="Carburants">\n\n        <div class="Carburant">\n\n          <input type="radio" id="Carburant1" name="Carburant" value="Carburant1" checked>\n\n          <label for="Carburant1">Carburant1</label>\n\n        </div>\n\n        <div class="Carburant">\n\n          <input type="radio" id="Carburant2" name="Carburant" value="Carburant2">\n\n          <label for="Carburant2">Carburant2</label>\n\n        </div>\n\n        <div class="Carburant">\n\n          <input type="radio" id="Carburant3" name="Carburant" value="Carburant3">\n\n          <label for="Carburant3">Carburant3</label>\n\n        </div>\n\n        <div class="Carburant">\n\n          <input type="radio" id="Carburant4" name="Carburant" value="Carburant4">\n\n          <label for="Carburant4">Carburant4</label>\n\n        </div>\n\n      </div>\n\n\n\n    </div>\n\n    <div class="Modal-box-second">\n\n\n\n      <div class="SelectInfos tabs">\n\n        <div class="SelectInfos-left">\n\n          <div class="SelectInfos-bloc SelectInfos-IndicePrix current" data-tab="tab-3">\n\n            <p>\n\n              <img src="assets/imgs/price.png">\n\n              <span>Indice Station</span>\n\n            </p>\n\n          </div>\n\n          <div class="SelectInfos-bloc SelectInfos-Adresse" data-tab="tab-4">\n\n            <p>\n\n              <img src="assets/imgs/localization.png">\n\n              <span>\n\n                Adresse\n\n              </span>\n\n            </p>\n\n          </div>\n\n        </div>\n\n        <div class="SelectInfos-right">\n\n          <div class="SelectInfos-bloc SelectInfos-Services" data-tab="tab-2">\n\n            <p>\n\n              <img src="assets/imgs/service.png">\n\n              Services\n\n            </p>\n\n          </div>\n\n          <div class="SelectInfos-bloc SelectInfos-Horaires" data-tab="tab-1">\n\n            <p>\n\n              <img src="assets/imgs/hour.png">\n\n              <span>\n\n                Horaires\n\n              </span>\n\n            </p>\n\n          </div>\n\n        </div>\n\n      </div>\n\n      <div class="SelectInfos SelectInfos-tab">\n\n\n\n        <div id="tab-1" class="tab-content">\n\n          <div class="title">Horaires</div>\n\n          <p class="text-tabs fullwidth">\n\n            Lundi : 8h - 19h\n\n            <br />\n\n            Mardi : 8h - 19h\n\n            <br />\n\n            Mercredi : 8h - 19h\n\n            <br />\n\n            Jeudi : 8h - 19h\n\n            <br />\n\n            Vendredi : 8h - 19h\n\n            <br />\n\n            Samedi : 8h - 19h\n\n            <br />\n\n            Dimanche : 8h - 19h\n\n          </p>\n\n        </div>\n\n        <div id="tab-2" class="tab-content">\n\n          <div class="title">Services</div>\n\n          <p class="text-tabs fullwidth">\n\n            Restauration, Pneus, Aspirateur, Supérette\n\n          </p>\n\n        </div>\n\n        <div id="tab-3" class="tab-content current">\n\n          <div class="title">Indice</div>\n\n          <img class="pic-tabs" src="assets/imgs/station_verte.png">\n\n          <br />\n\n          <p class="text-tabs">\n\n            Cette station est actuellement classée "Station verte" : elle a actuellement un prix moyen inférieur par rapport à ses concurrent.\n\n          </p>\n\n        </div>\n\n        <div id="tab-4" class="tab-content">\n\n          <div class="title">Adresse</div>\n\n          <p class="text-tabs fullwidth">\n\n            Adresse de la station\n\n          </p>\n\n        </div>\n\n\n\n      </div>\n\n\n\n    </div>\n\n  </div>\n\n</div>\n\n\n\n\n\n'/*ion-inline-end:"C:\Users\jessy\Documents\projets\fillintime\FillInTime-app\src\components\modal\modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_api_api__["a" /* ApiProvider */]])
    ], ModalComponent);
    return ModalComponent;
}());

//# sourceMappingURL=modal.js.map

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the MapsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var MapsProvider = /** @class */ (function () {
    function MapsProvider(http) {
        this.http = http;
        console.log('Hello MapsProvider Provider');
        this.maps = null;
        this.markers = [];
        this.infoWindow = [];
    }
    MapsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], MapsProvider);
    return MapsProvider;
}());

//# sourceMappingURL=maps.js.map

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ApiProvider = /** @class */ (function () {
    function ApiProvider(http) {
        this.http = http;
        this.httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({
                'Content-Type': 'application/json'
            })
        };
        console.log('Hello ApiProvider');
    }
    /**
     * Récupère la liste des stations
     */
    ApiProvider.prototype.getAllStation = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get("http://apifillintime.tp.crea.pro/public/index.php/api/stations?page=1", _this.httpOptions).subscribe(function (data) {
                resolve(data["hydra:member"]);
            }, function (err) {
                console.log(err);
            });
        });
    };
    /**
     * Récupère une station à partir de son ID
     * @param id
     */
    ApiProvider.prototype.getStation = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get("http://apifillintime.tp.crea.pro/public/index.php/api/stations/" + id, _this.httpOptions).subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    /**
     * Récupère la liste de prix du pétrole brut
     */
    ApiProvider.prototype.getCrudeOilPrice = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get("http://apifillintime.tp.crea.pro/public/index.php/api/petroles?page=1", _this.httpOptions).subscribe(function (data) {
                resolve(data["hydra:member"]);
            }, function (err) {
                console.log(err);
            });
        });
    };
    ApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ApiProvider);
    return ApiProvider;
}());

//# sourceMappingURL=api.js.map

/***/ })

},[200]);
//# sourceMappingURL=main.js.map